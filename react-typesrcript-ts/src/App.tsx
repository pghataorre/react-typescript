import React, {Fragment, useState} from 'react';
import './App.css';

type formElem = React.FormEvent<HTMLFormElement>
interface ITodo {
  text: string
  complete: boolean
}

function App() {
  const [value, setValue] = useState<string>('');
  const [todos, setToDos] = useState<ITodo []>([]);

  const handleSubmit = (e: formElem):  void => {
    e.preventDefault();
    addTodo(value);
    setValue('');
  }
  
  const addTodo = (text: string): void => {
    const newTodos: ITodo[] = [...todos, {text: text, complete: false}];
    setToDos(newTodos);
  }

  const completeToDos = (index: number): void => {
    const newTodos: ITodo[] = [...todos];
    newTodos[index].complete = !newTodos[index].complete;
    setToDos(newTodos);
  }

  const removeToDos = (index: number) : void => {
    const filterTodos: ITodo[] = [...todos];
    filterTodos.splice(index,1 );
    setToDos(filterTodos);
  }

  const todoList = todos.map( (item: ITodo, index: number) => 
      <Fragment key={index}>
        <div>
          {item.text}
        </div>
        <button type="button" onClick={ () => completeToDos(index) }>{
          item.complete ? 'Complete' : 'Incomplete' 
        }</button>
        <button type="button" onClick={ () => removeToDos(index) }>Remove</button>
      </Fragment>
    )

  return (
    <Fragment>
      <h1>To do list</h1>
      <form onSubmit={ handleSubmit }>
        <input 
          type="text" 
          value={value} 
          onChange={e => setValue(e.target.value)} 
          required
        />
        <button type="submit">Add Task - To do</button>
      </form>
      <section>
        { todoList }
      </section>
    </Fragment>
  );
}

export default App;
